package it.kdde.sequentialpatterns.model.list;

import it.kdde.sequentialpatterns.model.Itemset;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItems;

/**
 * Created by fabiofumarola on 17/11/14.
 */
public class ItemsetTest {


    @Test
    public void cloneTest() {
        Itemset itemset = new Itemset("a", "b", "c");
        Itemset cloned = itemset.clone();
        assert (itemset != cloned);
        assertEquals(itemset.concatenate(), cloned.concatenate());
    }


    @Test
    public void addTest() {
        Itemset itemset = new Itemset("a", "b", "c");
        Itemset other = itemset.clone();
        other.addItem("d", "e");
        assert itemset.size() + 2 == other.size();
    }

    @Test
    public void testEquality(){
        Itemset a =  new Itemset("1","2","3");
        Itemset b =  new Itemset("1","2","3");
        Itemset c = a.clone();

        assert a.equals(b) == true;
        assert a.equals(c) == true;
    }

    @Test
    public void testConcatenate(){
        Itemset a =  new Itemset("1","2","3");

        assert a.concatenate().equals("1 2 3");
    }

    @Test
    public void testContains(){
        Itemset a =  new Itemset("1","2","3");
        Itemset b =  new Itemset("1","3");

        assert a.contains(b);
        assert !b.contains(a);
    }
}
