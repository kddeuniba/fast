package it.kdde.sequentialpatterns.apriori;

import it.kdde.sequentialpatterns.model.FrequentItemset;
import it.kdde.sequentialpatterns.model.Sequence;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by fabiofumarola on 20/11/14.
 */
public class FrequentLitemsetExtractorTest {

    @Test
    public void extractFrequentItemsets() throws IOException {

        Path path = Paths.get("src/test/resources/contextPrefixSpan.txt");
        AprioriDataset aprioriDataset = AprioriDataset.fromPrefixSpanSource(path, 0.2F);

        FrequentLitemsetExtractor extractor = new FrequentLitemsetExtractor(aprioriDataset);
        List<FrequentItemset> result =  extractor.litemsetPhase();
        
        assert result.size() > 0;

        for (FrequentItemset frequentItemset : result) {
            System.out.println(frequentItemset);
            assert frequentItemset.getSupport() >= aprioriDataset.getAbsoluteSupport();
        }

    }
}
