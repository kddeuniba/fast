package it.kdde.sequentialpatterns.model.list;

import it.kdde.sequentialpatterns.model.Itemset;
import it.kdde.sequentialpatterns.model.Sequence;
import org.junit.Test;

/**
 * Created by fabiofumarola on 23/11/14.
 */
public class SequenceTest {

    @Test
    public void subSequenceTest1(){
        Sequence seq1 = new Sequence(new Itemset("a","b"), new Itemset("b"), new Itemset("c"));
        Sequence seq2 = new Sequence(new Itemset("a"), new Itemset("b"), new Itemset("c"));

        assert seq1.contains(seq2) == true;
    }

    @Test
    public void subSequenceTest2(){
        Sequence seq1 = new Sequence(new Itemset("a","b"), new Itemset("b"), new Itemset("c"));
        Sequence seq2 = new Sequence(new Itemset("a","b"), new Itemset("b"), new Itemset("c"));

        assert seq1.contains(seq2) == true;
    }

    @Test
    public void subSequenceTest3(){
        Sequence seq1 = new Sequence(new Itemset("a","b"), new Itemset("b"), new Itemset("c"));
        Sequence seq2 = new Sequence(new Itemset("a"), new Itemset("c"));

        assert seq1.contains(seq2) == true;
    }

    @Test
    public void subSequenceTest4(){
        Sequence seq1 = new Sequence(new Itemset("a","b"), new Itemset("b"), new Itemset("c"));
        Sequence seq2 = new Sequence(new Itemset("a","b"), new Itemset("c"));

        assert seq1.contains(seq2) == true;
    }

    @Test
    public void subSequenceTest5(){
        Sequence seq1 = new Sequence(new Itemset("a","b"), new Itemset("b"), new Itemset("c"));
        Sequence seq2 = new Sequence(new Itemset("a","b"), new Itemset("d"));

        assert seq1.contains(seq2) == false;
    }

    @Test
    public void subSequenceTest6(){
        Sequence seq1 = new Sequence(new Itemset("1"), new Itemset("8","2","1","7","3"), new Itemset("1","3"), new Itemset("4"), new Itemset("6","3"));
        Sequence seq2 = new Sequence(new Itemset("4"), new Itemset("1"), new Itemset("2"), new Itemset("1"), new Itemset("1"), new Itemset("1"));

        assert seq1.contains(seq2) == false;

    }

    @Test
    public void subSequenceTest7(){
        Sequence seq1 = new Sequence(new Itemset("1"), new Itemset("8","2","1","7","3"), new Itemset("1","3"), new Itemset("4"), new Itemset("6","3"));
        Sequence seq2 = new Sequence(new Itemset("4"), new Itemset("1"), new Itemset("2"), new Itemset("1"), new Itemset("1"));

        assert seq1.contains(seq2) == false;

    }
}
