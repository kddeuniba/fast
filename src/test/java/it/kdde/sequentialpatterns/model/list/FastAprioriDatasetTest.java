package it.kdde.sequentialpatterns.model.list;

import it.kdde.sequentialpatterns.fast.FastDataset;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by fabiofumarola on 18/11/14.
 */
public class FastAprioriDatasetTest {

    @Test
    public void loadPrefixSpan02F() throws IOException {
        Path path = Paths.get("src/test/resources/contextPrefixSpan.txt");
        FastDataset ds = FastDataset.fromPrefixspanSource(path, 0.2F);

        assert ds.getFrequentItemsets().size() == 7;
        assert ds.getNumRows() == 4;
        assert ds.getFrequentItemsets().get("1").getAbsoluteSupport() == 4;
        assert ds.getFrequentItemsets().get("5").getAbsoluteSupport() == 3;
        assert ds.getFrequentItemsets().get("7").getAbsoluteSupport() == 1;

    }

    @Test
    public void loadPrefixSpan1F() throws IOException {
        Path path = Paths.get("src/test/resources/contextPrefixSpan.txt");
        FastDataset ds = FastDataset.fromPrefixspanSource(path, 1F);

        assert ds.getFrequentItemsets().size() == 3;
        assert ds.getNumRows() == 4;
        assert ds.getFrequentItemsets().get("1").getAbsoluteSupport() == 4;
        assert ds.getFrequentItemsets().get("2").getAbsoluteSupport() == 4;
        assert ds.getFrequentItemsets().get("3").getAbsoluteSupport() == 4;
        assert ds.getFrequentItemsets().get("4") == null;
    }

    @Test
    public void testLoadingSparseIdList1() throws IOException {
        Path path = Paths.get("src/test/resources/contextPrefixSpan.txt");
        FastDataset ds = FastDataset.fromPrefixspanSource(path, 0.2F);

        assert ds.getSparseIdList("1").getElement(0,0).getColumn() == 1;
        assert ds.getSparseIdList("1").getElement(0,1).getColumn() == 2;
        assert ds.getSparseIdList("1").getElement(0,2).getColumn() == 3;

        assert ds.getSparseIdList("1").getElement(1,0).getColumn() == 1;
        assert ds.getSparseIdList("1").getElement(1,1).getColumn() == 4;

        assert ds.getSparseIdList("1").getElement(2,0).getColumn() == 2;
        assert ds.getSparseIdList("1").getElement(3,0).getColumn() == 3;
    }
}
