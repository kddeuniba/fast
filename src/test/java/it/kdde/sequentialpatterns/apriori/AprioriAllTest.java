package it.kdde.sequentialpatterns.apriori;

import it.kdde.sequentialpatterns.model.FrequentSequence;
import it.kdde.sequentialpatterns.model.Sequence;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by fabiofumarola on 23/11/14.
 */
public class AprioriAllTest {

    @Test
    public void findSequences() throws IOException {
        Path path = Paths.get("src/test/resources/contextPrefixSpan.txt");
        AprioriDataset aprioriDataset = AprioriDataset.fromPrefixSpanSource(path, 0.2F);

        AprioriAll aprioriAll = new AprioriAll(aprioriDataset, AprioriAll.PatternsType.FREQUENT);
        List<FrequentSequence> sequences = aprioriAll.run();

        assert sequences.size() == 812;

        //it should contains the original dataset sequences
        for (Sequence sequence : aprioriDataset) {
            boolean value = sequences.contains(sequence);
            assert value == true;
        }
    }

    @Test
    public void findClosedSequences() throws IOException {

        Path path = Paths.get("src/test/resources/contextPrefixSpan.txt");
        AprioriDataset aprioriDataset = AprioriDataset.fromPrefixSpanSource(path, 0.2F);

        AprioriAll aprioriAll = new AprioriAll(aprioriDataset, AprioriAll.PatternsType.CLOSED);
        List<FrequentSequence> sequences = aprioriAll.run();

        assert sequences.size() > 0;

        for (FrequentSequence sequence : sequences) {
            System.out.println(sequence);
        }


        //it should contains the original dataset sequences
//        for (Sequence sequence : aprioriDataset) {
//            boolean value =  sequences.contains(sequence);
//            assert  value == true;
//        }
    }
}
