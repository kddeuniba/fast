package it.kdde.sequentialpatterns.model.list;

import it.kdde.sequentialpatterns.fast.FastDataset;
import it.kdde.sequentialpatterns.model.SparseIdList;
import it.kdde.sequentialpatterns.model.VerticalIdList;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by fabiofumarola on 18/11/14.
 */
public class SparseIdListTest {

    private FastDataset ds;

    @Before
    public void before() throws IOException {
        Path path = Paths.get("src/test/resources/contextPrefixSpan.txt");
        ds = FastDataset.fromPrefixspanSource(path, 0.2F);
    }

    @Test
    public void addElement() {
        SparseIdList sil = new SparseIdList(10);
        sil.addElement(0, 2, null);
        assert sil.getAbsoluteSupport() == 1;
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addElementWrongColumn() {
        SparseIdList sil = new SparseIdList(10);
        sil.addElement(10, 2, null);
    }

    @Test
    public void iStep() {
        SparseIdList silA = ds.getSparseIdList("1");
        SparseIdList silB = ds.getSparseIdList("2");

        SparseIdList silC = SparseIdList.IStep(silA, silB);

        assert silC.getAbsoluteSupport() == 2;
        assert silC.length() == 4;

    }

    @Test
    public void iStepOneRow() {
        SparseIdList silA = new SparseIdList(1);
        silA.addElement(0, 1, null);
        silA.addElement(0, 2, null);
        SparseIdList silB = new SparseIdList(1);
        silB.addElement(0, 1, null);
        silB.addElement(0, 2, null);

        SparseIdList silC = SparseIdList.IStep(silA, silB);

        assert silC.getAbsoluteSupport() == 1;
    }

    @Test
    public void getStartingVil() {
        VerticalIdList vil = ds.getSparseIdList("1").getStartingVIL();

        assert vil.getElements()[0].getColumn() == 1;
        assert vil.getElements()[1].getColumn() == 1;
        assert vil.getElements()[2].getColumn() == 2;
        assert vil.getElements()[3].getColumn() == 3;
    }

    @Test
    public void getNextStartingVil() {
        VerticalIdList vil = ds.getSparseIdList("1").getStartingVIL();

        assert vil.getElements()[0].next().getColumn() == 2;
        assert vil.getElements()[1].next().getColumn() == 4;
        assert vil.getElements()[2].next() == null;
        assert vil.getElements()[3].next() == null;
    }
}